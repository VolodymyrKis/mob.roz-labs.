﻿//
//  ViewController.swift
//  Task1
//
//  Created by Volodymyr on 3/13/19.
//  Copyright © 2019 Volodymyr. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var newView: UIImageView!
    
    var label: UILabel!
    
    var baton: UIButton!
    
    var isHappy: Bool = false
    
    var squareFrame: CGRect {
        let midX = self.view.bounds.midX
        let midY = self.view.bounds.midY
        let size: CGFloat = 64
        return CGRect(x: midX-size/2, y: midY-size/2, width: size, height: size)
    }
    
    var ivFrame: CGRect {
        let w = self.view.frame.width
        let h = self.view.frame.height
        
        return CGRect(x: 0, y: 0, width: w, height: h/2)
    }
    
    var controllColour: UIColor {
        return UIColor( red: CGFloat(45/255.0), green: CGFloat(68/255.0), blue: CGFloat(115/255.0), alpha: CGFloat(1.0) )
    }
    
    override func loadView() {
        super.loadView()
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let statusbarHeight = UIApplication.shared.statusBarFrame.size.height
        
        newView = UIImageView()
        label = UILabel()
        baton = UIButton()
        
        baton.backgroundColor = .clear
        baton.layer.cornerRadius = 8
        baton.layer.borderWidth = 1
        baton.layer.borderColor = controllColour.cgColor
        baton.setTitleColor(controllColour, for: .normal)
        
        baton.setTitle("Let cat smile!", for: .normal)
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
        self.baton.addGestureRecognizer(gesture)
        
        
        /////
        label.textColor = controllColour
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont(name: label.font.fontName, size: 20)
        label.text = "I'am happy cat!"
        
        /////
        newView.backgroundColor = UIColor.red
        newView.layer.cornerRadius = 8
        
        newView.image = UIImage(named: "sad.jpg")
        
        view.addSubview(newView)
        view.addSubview(label)
        view.addSubview(baton)
        
        newView.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        baton.translatesAutoresizingMaskIntoConstraints = false
        
        
        let horizontalConstraint = NSLayoutConstraint(item: newView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 8)
        
        let verticalConstraint = NSLayoutConstraint(item: newView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -8)
        
        let topConstraint = NSLayoutConstraint(item: newView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: statusbarHeight)
        
        let labelLeft = NSLayoutConstraint(item: label, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 8)
        
        let labelRight = NSLayoutConstraint(item: label, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 8)
        
        let labelTop = NSLayoutConstraint(item: label, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: newView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 20)
        
        let batonLeft = NSLayoutConstraint(item: baton, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 8)
        
        let batonRight = NSLayoutConstraint(item: baton, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -8)
        
        let batonBottom = NSLayoutConstraint(item: baton, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -20)
        
        let widthConstraint = NSLayoutConstraint(item: newView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.view.frame.width)
        let heightConstraint = NSLayoutConstraint(item: newView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.view.frame.height/2)
        
        NSLayoutConstraint
            .activate([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint, topConstraint, labelLeft, labelRight, labelTop, batonLeft, batonRight, batonBottom])
    
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        isHappy = !isHappy
        if isHappy {
            newView.image = UIImage(named: "happy.jpg")
            baton.setTitle("Okay", for: .normal)
        } else {
            newView.image = UIImage(named: "sad.jpg")
            baton.setTitle("Let cat smile!", for: .normal)
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }

}

