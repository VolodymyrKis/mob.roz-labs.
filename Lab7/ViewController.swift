﻿//
//  ViewController.swift
//  Lab7
//
//  Created by Volodymyr on 3/3/19.
//  Copyright © 2019 Volodymyr. All rights reserved.
//

import UIKit

extension UIView {
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        next?.touchesBegan(touches, with: event)
    }
    
    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        next?.touchesEnded(touches, with: event)
    }
}

class ViewController: UIViewController {
    
    
    @IBOutlet weak var rectView: UIView!
    
    @IBOutlet weak var button: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: view)
            rectView.frame.origin.x = position.x
            rectView.frame.origin.y = position.y
        }
    }

    
    @IBAction func onTouchDown(_ sender: Any) {
        print("\(rectView.frame) : \(rectView.bounds)")
    }
    
}

